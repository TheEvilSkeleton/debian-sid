# Debian Sid Toolbox

Debian Sid Toolbox image. This image is meant to be used with the `toolbox` command.

## Installation

```
toolbox create -i registry.gitlab.com/theevilskeleton/debian-toolbox-sid
```

## Remove

```
toolbox rmi -f debian-toolbox-sid
```
